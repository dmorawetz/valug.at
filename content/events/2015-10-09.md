---
date: 2023-07-24 09:05:55+02:00
end: 2015-10-09 23:00:00+02:00
headcount: 9
location: VALUG-Klubraum, Alter Schl8hof Wels
start: 2015-10-09 18:30:00+02:00
subtitle: ''
tags: []
title: "VALUG Stammtisch - \xDCberarbeitung und Aktualisierung unserer Infrastruktur"

---
Wir nehmen uns der Infrastruktur (Wiki, Kalender…) an, migrieren und aktualisieren wo nötig.