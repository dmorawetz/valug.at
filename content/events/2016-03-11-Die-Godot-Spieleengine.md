---
date: '2016-03-18 15:45:29'
end: 2016-03-11 23:00:00+01:00
headcount: 12
location: VALUG-Klubraum, Alter Schl8hof Wels
slug: 2016-03-11-Die-Godot-Spieleengine
start: 2016-03-11 18:30:00+01:00
title: VALUG Stammtisch - Die Godot Spieleengine

---
## Linksammlung
 * [Offizielle_Webseite](http://godotengine.org/)
 * [Spiele-Engine_Godot_in_Version_2.0_freigegeben_(Pro-Linux)](http://www.pro-linux.de/news/1/23284/spiele-engine-godot-in-version-20-freigegeben.html)

## Weiterführende Infos
 * Eine weitere Game Engine mit ähnlicher Funktionsweise wurde im Quellcode freigegeben: [Atomic_Game_Engine](http://atomicgameengine.com/blog/announcement-2/)
