---
date: '2017-05-07 15:16:59'
end: 2017-04-14 23:00:00+02:00
headcount: 15
location: VALUG-Klubraum, Alter Schl8hof Wels
slug: 2017-04-14-Ein-Blick-auf-SSH
start: 2017-04-14 18:30:00+02:00
title: VALUG Vortrag - SSH

---
* Vortrag von notizblock
* [Folien](https://nblock.org/static/talks/slides-valug-ssh.pdf)
