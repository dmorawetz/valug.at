---
end: 2019-12-13 23:00:00+01:00
start: 2019-12-13 18:30:00+01:00
title: VALUG Stammtisch - The Yocto Project

---
It's not an embedded Linux Distribution, It creates a custom one for you.

Es wird eine kurze Einführung und etwas Einblick über Yocto geben, und über dessen Einsatz von ca. einem gutem Jahr.

Danach wird noch kleines Praxisbeispiel hergezeigt und ein fertiges Yocto Image auf einem libreH3(vergleichbar mit einen Banana oder Rasberry pi) Board bespielt.