---
date: '2019-01-12 11:34:14'
end: 2019-01-11 23:00:00+01:00
headcount: 8
location: VALUG-Klubraum, Alter Schl8hof Wels
slug: 2019-01-11-Games
start: 2019-01-11 18:30:00+01:00
title: VALUG Stammtisch - Thema noch nicht fixiert

---
<code>
Valug 2019-01-11
================
Kein geplantes Thema

Mitschrift auf https://public.etherpad-mozilla.org/p/valug-2019-01

# Bemerkenswert
Vortragsthemen schon bis zum Juli fixiert

# Spielesammlung für Linux
{{% comment %}} OpenRA{{% /comment %}}
Engine reimplementierung für alte Westwood Spiele (Command and Conquer Reihe)  
Mods: https://moddb.com

* Medieval Warefare
* Nomad Galaxy, noch in Entwicklung

{{% comment %}} Widelands{{% /comment %}}
 mit Grafiken von faang, ähnlich wie Siedler 2
 
{{% comment %}} Autonauts{{% /comment %}}
Wirtschaftsspiel mit Roboter, die angelernt und betreut werden wollen
Gratis early access  
https://denki.itch.io/autonauts


{{% comment %}} Open TTD{{% /comment %}}
Kassischer Transport Simulator
Grafikpaket per Menü im Spiel nachinstallieren


{{% comment %}} Unknown Horizons{{% /comment %}}
wurde heute nach fast 2 Jahren neue Release veröffentlicht  
ist Anno 1602 Clon


{{% comment %}} Super Tux Cart  {{% /comment %}}
heute neue Release mit online Multiplayer, V0.10-beta


{{% comment %}} DustRacing2D  {{% /comment %}}
Top View Autorennspiel, Computergegner,  
https://github.com/juzzlin/DustRacing2D  
Physik-Anpassungen von Karl auf Anfrage erhältlich


{{% comment %}} Snow Topia{{% /comment %}}
Schigebiet-Simulator, top 3D-Grafik


{{% comment %}} Cultivation {{% /comment %}}
2D unterwasser Spiel  
https://www.youtube.com/watch?v=9UkNWAGoqoE


{{% comment %}} The Secret Chronicles of Dr. M. {{% /comment %}}
https://secretchronicles.org/en/
Nettes Jump'n'Run Spiel, ähnlich Super Mario

{{% comment %}} Agar{{% /comment %}}
Web-Game: Kreise fressen Kreise, ähnlich zu Staub-Partikel im All

Clon: https://github.com/huytd/agar.io-clone

{{% comment %}} Koules{{% /comment %}}
Ähnlich zu Agar, jedoch aus dem Jahre Schnee!  
https://www.ucw.cz/~hubicka/koules/English/screenshots.html

{{% comment %}} Planet Blupi{{% /comment %}}
Baue eine Welt mit gelben Männchen  
http://www.ceebot.com/blupi/planet-e.php


# Spielesammulung für Android über F-Droid
{{% comment %}} Rabbit Escape{{% /comment %}}
ähnlich zu Lemminge, Bleistiftskizzengrafik

{{% comment %}} Anuto TD{{% /comment %}}
Tower Defense Spiel mit Suchtpotential und Bleistiftskizzengrafik, 
sehr Gewaltarme Darstellung

{{% comment %}} Turo{{% /comment %}}
Bau einen Turm, der nicht umfällt, aber Ziele am Weg nach oben abdeckt.

{{% comment %}} Tower Jumper{{% /comment %}}
Hüpfender Farbklex muss eine Spindel hinabhüpfen. Die Spindel hat Ebenen mit 
Lücken und kann gedreht werden, orange Bereiche dürfen nicht berührt werden. 

{{% comment %}} Crack-Attack (Play Store){{% /comment %}}
Eier zerschießen, 
""Really well-presented; a nice twist on the matching genre"




# LAN Agenda (März 2019)
mit Angaben zu Rundenspieldauern

* Supertux Kart v0.10 (ca. 5 Minuten)
* Widelands (lange Spielzeit, ca. 5 Stunden)
* OpenTTD (lange Spielzeit, ca. 5 Stunden)
* OpenRA (30 Minuten bis 1 Stunde)
* Unknown Horizons (lange Spielzeit, ca. 5 Stunden)
* 0 A. D. (30 Muten bis 1 Stunde)
* Dust Racing 2D (ca. 5 Minuten)
* Overload
* Open Arena (ca. 15 minuten)
* Xonotic (ca. 15 Minuten)
* Minetest (Endlos)
* openra
* Hedgewars
* OpenLiero
* Teeworld
* Planet Blupi
* Atomic Bomberman oder Bomberclone


# Sonstige Empfehlungen und Kommentare

{{% comment %}} Firefox Plugin{{% /comment %}}
No Script
Blockiert Java Script, kann individuell angepasst werden bei jedem 
Seitenbesuch. Typische Tracker können somit von vornherein deaktiviert 
werden, Extrembeispiel ist z.B. Heise.de

{{% comment %}} Datei-Synchronisierung mit Seafile{{% /comment %}}
auf eigenem Server

{{% comment %}} Manjaro{{% /comment %}}
wurde im letzen Stammtisch empfohlen, auch für neue Linuxuser. 
Karls Kurzerfahrung dazu (im Vergleich zu Linux Mint 19): 
Geht grundsätzlich gut, aber einiges machte Probleme:

* Seafile Client konnte nicht kompiliert werden, läuft auf Ubuntu/Mint völlig problemlos
* Shutter (Screen Shot Programm mit Editor) konnte nicht zum Laufen gebracht werden. 
* Manjaro kommt mit Dark-Theme, bei Umstellung auf Light-Theme bleiben manche Apps im Dark-Theme (z.B. KeePassXC)
* AppImage für Prusa's Slic3r: GUI ist gebrochen, Buttons und Checkboxes teilweise nicht sichtbar

{{% comment %}} WLAN-Router mit OpenWRT{{% /comment %}}
Hersteller GL-iNet
silwol hat zB GL-AR750

{{% comment %}} Fairphone 2{{% /comment %}}
kann von fraag nicht empfohlen werden
funktioniert technisch nicht, Support antwortet nicht. 
</code>
