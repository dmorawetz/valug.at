---
date: '2010-10-08 19:04:55'
end: 2010-10-08 23:55:00+02:00
headcount: 7
location: Alter Schl8hof Wels
slug: "2010-10-08-Python-Einf\xFChrung"
start: 2010-10-08 18:30:00+02:00
title: VALUG-Stammtisch

---
# Python Einführung
Vortragender: Albert Brandl

## Daten
 * [Vortragsfolien (PDF)](vortrag.pdf)
 * [Source-Dateien (tar.gz)](Daten.tar.gz)
