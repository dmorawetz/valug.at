---
date: '2022-06-11 11:18:41'
title: WireGuard, Headscale und Tailscale
slug: 2022-06-11-tailscale
start: '2022-06-10 18:30:00'
location: Alter Schlachtraum, Wels
---
## Ziele
 * Zugriff auf verschiedene Systeme
  * öffentliche Server
  * private Server
  * Laptops/Desktops
  * Drucker
  * IoT (Wechselrichter, …)
 * Zugriff über verschiedene Netzwerkstacks
  * öffentliche, statisch IPv4/IPv6 (Server)
  * Dynamische, externe IP (ADSL)
  * Carrier-Grade-NAT (CGN)
 * Meta
  * FOSS
  * Selfhosted
  * Gewartet/maintained
  * Zentrale Wartung
  * Access Control zentral
  * Kommunikation dezentral (Daten sollen nicht über einen zentralen Knoten geroutet werden)

## Mögliche Lösungen
 * Cloud Services nutzen
 * Zentrales VPN (OpenVPN, …)
 * Tinc: https://www.tinc-vpn.org/
 * Nebula: https://github.com/slackhq/nebula
 * Zerotier: https://zerotier.com/
 * Tailscale/Headscale


## Tailscale
 * https://tailscale.com/
  * https://tailscale.com/blog/how-tailscale-works/
  * https://tailscale.com/blog/how-nat-traversal-works/
 * "Networking like in a 90s LAN"
 * Komponenten
  * Client: Open Source, Windows, Linux, Mac, iOS, Android, …
  * Management Service: Closed Source, Cloud-based --> Headscale
  * Global verteilte Relay Server
 * Zentrales Management (Geräteverwaltung, …):
 * Access-Control-Lists (ACL, Firewalling): https://tailscale.com/kb/1018/acls/
 * Dezentrale Kommunikation (basierend auf Wireguard - Go Implementierung, nicht Kernel)
 * Statische IPv4 und statische IPv6 für jedes Gerät
 * MagicDNS: device.namespace.example.com -> 100.64.x.x
 * Subnet Router
 * Exit Node (route all traffic): https://tailscale.com/kb/1103/exit-nodes/
 * Nat punching - DERP (Relay Server)
 * File Sharing zwischen Nodes

## Headscale
 * https://github.com/juanfont/headscale
 * Open Source Implementierung des Tailscale Coordination Servers
 * Unabhängig von Tailscale Inc.
 * Selfhosted
 * Geschrieben in Go
 * Alle paar Monate ein Release

## Hinweise
 * Jeder hat - laut RTR - ein Recht auf eine öffentliche IP-Adresse.
 * Definition: "Was ist ein Internetanschluss?"
 * Derzeit ist nur 1 Tailscale Netzwerk möglich: https://github.com/tailscale/tailscale/issues/183

## Links und Notizen
 * cve mitre
 * https://sec-consult.com/vulnerability-lab/advisory/multiple-vulnerabilites-in-fronius-solar-inverter-series-cve-2019-19229-cve-2019-19228/
 * https://github.com/jncronin/rpi-boot
 * Karl Zeilhofer: https://www.youtube.com/user/lookatthisvidsandfun
