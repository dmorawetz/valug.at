---
date: '2014-11-15 08:20:12'
end: 2014-11-14 23:00:00+01:00
headcount: 8
location: VALUG-Klubraum, Alter Schl8hof Wels
slug: 2014-11-14-sporttracking
start: 2014-11-14 18:30:00+01:00
title: 'VALUG Stammtisch mit Vortrag + Diskussion: Sporttracker unter Linux/Android'

---
Vortrag von notizblock

## Inhalt
 * Ein Abriss über die letzten Monate im Bereich Fitnesstracking
 * Gefahren (Zugriff auf Daten, Was lässt sich auswerten, …)
 * Ziele und Möglichkeiten
 * Tools ohne Onlinezwang
 * Demo von MyTourbook
 * Fazit

## Slides
 * **Git-Repository:** https://gitorious.org/valug/notizblock-sporttracker-slides
 * **PDF:** [Download](http://nblock.org/static/talks/slides-sportstracker.pdf)
 * **Notizen:** [Download](http://nblock.org/static/talks/notes-sportstracker.txt)
