---
end: 2022-05-13 23:30:00+02:00
location: VALUG Klubraum im Alten Schl8hof
start: 2022-05-13 18:30:00+02:00
title: VALUG-Stammtisch - Vaultwarden / Bitwarden

---
silwol zeigt Vaultwarden mit Bitwarden-Frontend her.

Zum ersten Mal im heurigen Jahr treffen wir uns wieder vor Ort im VALUG-Klubraum im Alten Schl8hof.