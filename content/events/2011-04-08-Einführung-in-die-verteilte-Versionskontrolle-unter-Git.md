---
date: '2011-04-08 21:05:20'
end: 2011-04-08 22:00:00+02:00
headcount: 5
location: VALUG-Klubraum im Alten Schl8hof Wels
slug: "2011-04-08-Einf\xFChrung-in-die-verteilte-Versionskontrolle-unter-Git"
start: 2011-04-08 18:30:00+02:00
title: Einstieg in die verteilte Versionskontrolle mit Git
---
Workshop mit notizblock und silwol.

Themen:
 * Eigene Git-Repositories anlegen
 * Fremde Git-Repositories klonen
 * Git-Repositories veröffentlichen
 * Verteilte Zusammenarbeit, Zusammenführen von Änderungen aus mehreren Repositories

## Slides
 * **Git-Repository:** https://gitorious.org/valug/git-slides
 * **PDF:** [Download](slides-git.pdf)
