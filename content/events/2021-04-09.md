---
end: 2021-04-09 23:30:00+02:00
location: Online
start: 2021-04-09 18:30:00+02:00
title: VALUG-Stammtisch

---
Wir treffen uns online im Jitsi-Raum unter https://meet.apronix.net/valug

Thema ist alles was ihr in den Raum bringt. Zusätzlich werden wir ein wenig über https://github.com/latchset/clevis und https://github.com/latchset/tang und damit verbunden über automatisiertes Enstperren von verschlüsselten Datenträgern sprechen.