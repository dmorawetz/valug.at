---
date: '2016-05-14 11:05:31'
end: 2016-05-13 23:00:00+02:00
headcount: 9
location: VALUG-Klubraum, Alter Schl8hof Wels
slug: 2016-05-13-Webscraping-mit-Scrapy
start: 2016-05-13 18:30:00+02:00
title: VALUG Vortrag - Webscraping mit Scrapy

---
Vortrag von notizblock

## Inhalt
 * Was ist Webscraping
 * Anwendungsgebiete
 * Scrapy
 * Demos
 * Diskussion

## Slides
 * **Git-Repository: https://gitlab.com/valug/notizblock-scrapy-slides** 
 * **Folien:** [Download](http://nblock.org/static/talks/slides-scrapy.pdf)
 * **Demos:** https://gitlab.com/valug/notizblock-scrapy-demos
