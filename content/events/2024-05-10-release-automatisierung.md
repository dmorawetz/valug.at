---
title: VALUG-Treffen - Release Automatisierung
start: 2024-05-10 18:30:00+01:00
end: 2024-05-10 23:30:00+01:00
location: VALUG-Klubraum, Alter Schl8hof Wels
---

Silwol ist für das Release Management bei OpenTalk zuständig und zeigt wie Release Management dort funktioniert. Im
Vortrag geht es um die etablierten Prozesse, die verwendete Software Tools und die Automatisierung bei der Erstellung
von Releases.
