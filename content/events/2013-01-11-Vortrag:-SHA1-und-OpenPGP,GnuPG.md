---
date: '2013-01-13 12:13:40'
end: 2013-01-11 22:30:00+01:00
headcount: 10
location: VALUG-Klubraum, Alter Schl8hof Wels
slug: 2013-01-11-Vortrag:-SHA1-und-OpenPGP,GnuPG
start: 2013-01-11 18:30:00+01:00
title: SHA1 und OpenPGP/GnuPG
---
Vortrag

Themen:
 * Was ist SHA-1?
 * Wo wird SHA-1 im OpenPGP Standard verwendet?
 * Wo wird SHA-1 in GnuPG verwendet?
 * Probleme und Interoperabilität
 * Diskussion
