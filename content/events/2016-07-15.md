---
date: 2023-07-24 09:05:55+02:00
end: 2016-07-15 23:00:00+02:00
headcount: 11
location: VALUG-Klubraum, Alter Schl8hof Wels
start: 2016-07-15 18:30:00+02:00
subtitle: ''
tags: []
title: VALUG Workshop - Tryton ERP

---
An Hand der aktuellen Version 4 der freien ERP-Suite Tryton bieten wir eine Übersicht über die Fähigkeiten des Systems.
- Stammdatenverwaltung von Parteien (Organisationen, Personen…) und Artikeln
- Einkauf, Verkauf
- Lagerverwaltung
- Produktion
- Buchhaltungen
- Installation und Administration des Systems
- Übersicht über spezielle Module (Chargennummern, Kostenberechnungen, Kreditlimit…)