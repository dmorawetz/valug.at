---
end: 2023-11-17 23:30:00+01:00
location: VALUG-Klubraum, Alter Schl8hof Wels
start: 2023-11-17 18:30:00+01:00
title: "VALUG-Treffen - FIDO2, WebAuthn, Passkey und ID Austria"

---

Das Ziel des Vortrags ist ein Überblick über das Thema "Passwortlose Authentifizierung" bzw. "Security Keys" zu geben
und wir werden folgende Bereiche besprechen:

* Was bedeuten die Begriffe FIDO2, WebAuthn, Passkey?
* Wie funktioniert die Authentifizierung mit Security Keys?
* Welche Arten von Schlüssel gibt es?
* Fallstricke bei der Verwendung von Security Keys

Wir besprechen auch die geplante Abschaltung der "Handy Signatur" am 5. Dezember 2023 und wie man auf "ID Austria" mit
Security Key wechseln kann.
