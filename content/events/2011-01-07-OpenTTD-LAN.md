---
date: '2011-01-11 20:58:58'
end: 2011-01-07 22:00:00+01:00
headcount: 9
location: VALUG-Clubraum im Alten Schlachthof in Wels
slug: 2011-01-07-OpenTTD-LAN
start: 2011-01-07 10:00:00+01:00
title: VALUG OpenTTD-LAN

---
Anwesend waren 9 Personen. Anfangs wurde ein dedizierter OpenTTD-Server installiert, danach wurde viele Stunden lang OpenTTD gespielt.

## Bilder
![attachment:DSC_6877_1.JPG](DSC_6877_1.JPG)

![attachment:DSC_6876_1.JPG](DSC_6876_1.JPG)

![attachment:DSC_6874_1.JPG](DSC_6874_1.JPG)

![attachment:DSC_6875_1.JPG](DSC_6875_1.JPG)

![attachment:DSC_6878_1.JPG](DSC_6878_1.JPG)

![attachment:DSC_6879_1.JPG](DSC_6879_1.JPG)

![attachment:DSC_6880_1.JPG](DSC_6880_1.JPG)
