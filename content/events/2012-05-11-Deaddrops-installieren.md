---
date: '2012-05-23 14:15:31'
end: 2012-05-11 23:30:00+02:00
headcount: 6
location: VALUG-Klubraum, Alter Schl8hof Wels
slug: 2012-05-11-Deaddrops-installieren
start: 2012-05-11 18:30:00+02:00
title: VALUG-Stammtisch

---
# Deaddrops installieren am 11.5.2012
Eine Gruppe VALUGer/innen haben am 11.5.2012 zwei Deaddrops installiert. Informationen über Deaddrops sind auf der Seite http://deaddrops.com/ zu finden. Die Orte der installierten Drops sind unter http://deaddrops.com/db/?page=view&id=1017 und http://deaddrops.com/db/?page=view&id=1018 zu finden.
