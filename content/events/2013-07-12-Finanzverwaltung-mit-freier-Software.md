---
date: '2013-07-13 12:53:54'
end: 2013-07-12 23:00:00+02:00
headcount: 10
location: VALUG-Klubraum, Alter Schl8hof Wels
slug: 2013-07-12-Finanzverwaltung-mit-freier-Software
start: 2013-07-12 18:30:00+02:00
title: Finanzverwaltung mit freier Software (GnuCash)

---
Workshop mit notizblock und s.n.

Themen:
 * Warum private Finanzverwaltung?
 * Vorstellung von GnuCash
 * Dateneingabe in GnuCash
 * Berichte und Auswertungen mit GnuCash
 * Showcase der wichtigsten Features für die private Nutzung
 * Diskussion

## Slides
Die Folien sowie Beispieldateien, Rechnung und Anleitungen stehen auf Anfrage gerne zur Verfügung.
