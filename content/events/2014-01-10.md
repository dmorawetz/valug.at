---
end: 2014-01-10 23:00:00+01:00
location: VALUG-Klubraum, Alter Schl8hof Wels
start: 2014-01-10 18:30:00+01:00
title: 'Diskussionsrunde: digitales Erben'

---
Ich habe Daten, die soll zu meinen Lebzeiten niemand ausser mir selbst zu Gesicht bekommen, daher liegen sie auf einer oder mehreren verschlüsselten Festplatten. Wenn ich jedoch verstorben bin, möchte ich diese Daten zuverlässig und gezielt bestimmten Personen zugänglich machen. Wir überlegen und diskutieren Konzepte, um diese gezielte Datenweitergabe zu ermöglichen.