---
date: 2023-07-24 09:05:55+02:00
end: 2017-02-10 23:00:00+01:00
headcount: 10
location: VALUG-Klubraum, Alter Schl8hof Wels
start: 2017-02-10 18:30:00+01:00
subtitle: ''
tags: []
title: VALUG Vortrag - Configuration Management & DevOps

---
Configuration Management? DevOps? Ansible? Puppet? Was ist das überhaupt und wofür kann ich diese Tools einsetzen? Wir geben einen Einblick in diese Tools und versuchen die Vor- und Nachteile bzw. Fallstricke aufzuzeigen. Geschichten aus der Praxis dürfen auch nicht fehlen.