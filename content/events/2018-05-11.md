---
end: 2018-05-11 22:00:00+02:00
location: Team14, Pettenbach
start: 2018-05-11 17:00:00+02:00
title: "VALUG Stammtisch - Elektronik-Best\xFCcker"

---
Diesmal treffen wir uns bei Team14 ab 17:00 zum Grillen an der Alm (türkiesblauer Gebirgsfluss).

Alles was auf den Grill soll, müsst ihr selbst mitnehmen, für Getränke ist gesorgt.

Danach gibts noch eine Führung durchs Labor und eine Vorstellung des Bestückungsautomaten, der mit der freien Software OpenPnP und Linux betrieben wird. Genaue Adresse auf http://www.team14.at/doku.php?id=kontakt
Location: https://osm.org/go/0JKudQVBM--?m=