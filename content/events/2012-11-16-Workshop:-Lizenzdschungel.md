---
date: '2012-11-17 10:05:38'
end: 2012-11-16 23:30:00+01:00
headcount: 10
location: VALUG-Klubraum, Alter Schl8hof Wels
slug: 2012-11-16-Workshop:-Lizenzdschungel
start: 2012-11-16 18:30:00+01:00
title: 'Workshop: Lizenzdschungel'

---
Workshop mit notizblock und silwol.

Themen:
 * Strong Copyleft, Weak Copyleft, Permissive Licenses
 * Welche freie Softwarelizenzen gibt es?
 * Wie unterscheiden sich die Lizenzen voneinander?
 * Welche Lizenz verwendet Projekt X?
 * Diskussion
