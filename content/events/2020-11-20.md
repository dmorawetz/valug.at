---
end: 2020-11-20 23:00:00+01:00
location: Online
start: 2020-11-20 18:30:00+01:00
title: VALUG Online-Stammtisch

---
Auf Grund der aktuellen Situation in Österreich und insbesondere Oberösterreich treffen wir uns wieder online unter https://meet.apronix.net/valug