---
end: 2020-06-12 23:00:00+02:00
location: Online
start: 2020-06-12 18:30:00+02:00
title: VALUG Online-Stammtisch - WebAssembly

---
Wir lernen etwas über WASM - WebAssembly

Treffpunkt online ab 18:30 unter https://meet.apronix.net/valug