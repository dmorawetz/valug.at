---
date: '2011-03-11 18:34:55'
end: 2011-03-11 22:00:00+01:00
headcount: 7
location: VALUG-Klubraum, Alter Schl8hof Wels
slug: 2011-03-11-CSharp-und-GTK
start: 2011-03-11 18:30:00+01:00
title: Einstieg in GTK-Programmierung mit C# und Monodevelop.
---

* Slides:
    * [Download PDF](slides-gtk-csharp.pdf)
    * [Git](http://gitorious.org/valug/csharp-gtk-workshop)
 * Beispiel-Sourcecode:
    * [Git](http://gitorious.org/valug/rss-reader-demo-csharp)
