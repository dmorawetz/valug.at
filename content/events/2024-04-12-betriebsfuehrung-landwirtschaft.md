---
title: VALUG-Treffen - Betriebsführung Landwirtschaft
start: 2024-04-12 14:00:00+01:00
end: 2024-04-12 23:30:00+01:00
location: Bushaltestelle Pichl bei Wels Ortsmitte
---

Dieses mal dürfen wir einen landwirtschaftlichen Betrieb besuchen! Gerald ermöglicht uns einen Blick in die moderne
Landwirtschaft und zeigt uns seinen Betrieb. Zu den Highlights zählen:

* Betriebsführung
* Funktionsweise der modernen Landwirtschaft
* Ein selbstgebautes Fahrerassistenzsystem basierend auf AgOpenGPS
* Eine nagelneuer Steyr CVT Traktor (sofern der Liefertermin hält)

## Organisatorische Hinweise
Wir treffen uns dieses mal um 14:00 in Pichl bei Wels, bei der Bushaltestelle "Pichl bei Wels Ortsmitte". Ein passender
Bus aus Wels kommt dort um 14:02 an. Von der Bushaltestelle ist es noch ein kurzer Fußweg zum Betrieb.
