---
date: '2021-06-13 11:23:25'
title: Index
---

# Die nächsten Termine

{{<upcoming-events count="3">}}

[**Alle Veranstaltungen**](/events)

[Kalender abonnieren](/events/index.ics)

# Was ist die VALUG?

Die VALUG ist ein Treffpunkt für Fans von freien Betriebssystemen und freier Software. Allen voran natürlich GNU/Linux-Fans, aber auch Anhänger/innen diverser BSD-Varianten finden sich immer wieder bei uns ein.

VALUG steht für ***Voralpen Linux User Group*** und bei uns ist jede:r willkommen, wir wollen möglichst viele Menschen erreichen.

Es gibt keine Einstiegshürden und es wird kein Vorwissen vorausgesetzt. Natürlich sind auch Profis willkommen und in bester Gesellschaft. Die Themenbereiche, die wir bei unseren Treffen, in [Mailinglisten](/pages/Mailingliste) oder im [Chat](/pages/Chat) behandeln, sind dementsprechend breit gefächert (siehe vergangene [Veranstaltungen](/events)).

Wir treffen regelmäßig vor Ort im [VALUG-Klubraum](/pages/Klubraum), meist am 2. Freitag des Monats. Die Termine sind unter [**Veranstaltungen**](/events) zu finden.

Wir sind auch online in Verbindung, meist im [Chat](/pages/Chat) oder über die [Mailingliste](/pages/Mailingliste).

# Über diese Webseite

Diese Website wird von [hugo](https://gohugo.io) gerendert, der [Inhalt](https://gitlab.com/valug/valug.at) liegt auf Gitlab. Bei Änderungsvorschlägen bitte einfach ein [Issue](https://gitlab.com/valug/valug.at/-/issues/new) oder einen [Merge Request](https://gitlab.com/valug/valug.at/-/merge_requests/new) erstellen.
