---
date: '2015-12-23 15:54:20'
title: "\xD6ffentlichkeitsarbeit"
---
# Öffentlichkeitsarbeit
Alle Dokumente und Unterlagen, welche die Öffentlichkeitsarbeit der VALUG betreffen, sind in einem öffentlichen [git-Repository](https://gitlab.com/valug-media/valug-media) gespeichert, das auf gitlab.com gehostet ist.

## Flyer und Poster

## Vorlage
 * Die aktuelle Vorlage für kommende Termine befindet sich im git-Repository [valug-media](https://gitlab.com/valug-media/valug-media/blob/master/flyer/template/upcoming-events-a4.odg).

## Bisherige Flyer und Poster
| **Datum** | **Download** |
| --- | --- |
| **März bis April 2011** | [A4 OpenDocument](/files/flyer_mar_apr_2011_a4.odg) , [A4 PDF](/files/flyer_mar_apr_2011_a4.pdf) |
| **Februar bis April 2011** | [A4 OpenDocument](/files/flyer_feb_mar_apr_2011_a4.odg), [A4 PDF](/files/flyer_feb_mar_apr_2011_a4.pdf), [A5 PDF](/files/flyer_feb_mar_apr_2011_a5.pdf), [A6 PDF](/files/flyer_feb_mar_apr_2011_a6.pdf) |
| **November + Dezember 2010** | [A4 OpenDocument](/files/flyer_nov_dez_2010_a4.odg), [A4 PDF](/files/flyer_nov_dez_2010_a4.pdf), [A5 PDF](/files/flyer_nov_dez_2010_a5.pdf), [A6 PDF](/files/flyer_nov_dez_2010_a6.pdf) |

 * Computer Course (not only) for refugees 2016: [computer-kurse.pdf](/files/computer-kurse.pdf)
