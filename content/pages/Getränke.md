---
date: '2019-07-12 19:08:44'
title: "Getr\xE4nke"
---
# Getränke im VALUG-Klubraum

Wir haben im [VALUG-Klubraum](/pages/Klubraum) meistens einige Kisten Getränke:
 * Limonaden
 * Mate

Die Getränke werden durch freiwillige Spenden finanziert.
